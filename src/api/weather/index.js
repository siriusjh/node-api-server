import express from "express";
import * as weather from "./weather";
const router = express.Router();

router.get("/getUltraSrtNcst", weather.getUltraSrtNcst);
router.get("/getVilageFcst", weather.getVilageFcst);
router.get("/getCurrentWeather", weather.getCurrentWeather);

export default router;