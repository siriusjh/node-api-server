import axios from "axios";
import convert from "xml-js";

export const getCurrentWeather = async (req, res) => {
  try {
    const { data } = await axios.get(
      `${process.env.SERVICE_URL_WEATHER_WM}?lat=35&lon=139&APPID=${process.env.SERVICE_KEY_WEATHER_WM}&units=metric`
    );
    console.log(data);
  } catch (e) {
    console.log(e);
  } finally {
    res.end();
  }
};

export const getUltraSrtNcst = async (req, res) => {
  try {
    const { status, data } = await axios.get(
      `${process.env.SERVICE_URL_WEATHER_VI}/getUltraSrtNcst?serviceKey=${process.env.SERVICE_KEY_WEATHER_VI}&numOfRows=10&pageNo=1&base_date=20200107&base_time=0600&nx=55&ny=127`
    );
    console.log(status);
    console.log(data);
    console.log(convert.xml2json(data));
  } catch (e) {
    const error = e.toJSON();
    console.log(error.message);
  } finally {
    res.end();
  }
};

export const getVilageFcst = async (req, res) => {
  try {
    const { status, data } = await axios.get(
      `${process.env.SERVICE_URL_WEATHER_VI}/getVilageFcst?serviceKey=${process.env.SERVICE_KEY_WEATHER_VI}&numOfRows=10&pageNo=1&base_date=20200107&base_time=0230&nx=55&ny=127`
    );
    console.log(status);
    console.log(data);
    console.log(convert.xml2json(data));
  } catch (e) {
    const error = e.toJSON();
    console.log(error.message);
  } finally {
    res.end();
  }
};
