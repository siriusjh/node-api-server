import express from "express";
import "./env";
import logger from "morgan";
import fs from "fs";
import path from "path";
import weather from "./api/weather";

const PORT = 4040;
const app = express();
app.use(logger("dev"));
app.use(express.static("static"));

app.get("/", (req, res) => {
  return res.sendFile(path.join(__dirname, "../views/index.html"));
});

app.use("/weather", weather);

const handleListening = () =>
  console.log(`server running : http://localhost:${PORT}`);

const server = app.listen(PORT, handleListening);
